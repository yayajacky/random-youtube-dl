const _progress = require('cli-progress');
const request = require('request-promise');
const fs = require('fs');
const ytdl = require('ytdl-core');
const later = require('later');
  
// const textSched = later.parse.text('every 30 sec');
later.date.localTime();
const sched2 = later.parse.recur()
  .every(7).minute().after(7).hour().before(16).hour()
.and()
  .every(1).minute().after(16).hour().before(23).hour();
  
let downloading = false;
const bar1 = new _progress.Bar({}, _progress.Presets.shades_classic);

const vidList = [
  'NpMFBv0Xzs8',
  '6dmTmP4Dsw8',
  '6TPyRLBoKDI',
  'erkGrQlby1c',
  '4bQ1BX3hz7I',
  '_Uu12zY01ts',
  '814gP_yHcAg',
  'nCDVEyKUKd8',
  'Tch4v0L0GHA',
  'As2-JwaH3BE',
  '0W5xKVNfPEo',
  'y_1U2wOgUk0',
  'dpwFJN51eSU',
  'fwaiNkokVng'
];

later.setInterval(() => {
  const next = later.schedule(sched2).next(1);
  console.log("Next interval time: " + next);
  if (downloading)
    return;
  
  const res = {
    vid: vidList[Math.floor(Math.random() * vidList.length)]
  };
  
  if (res.vid) {
    console.log(res);
    const video = ytdl(
      `http://www.youtube.com/watch?v=${res.vid}`,
      { filter: (format) => format.container === 'mp4' }
    )
    video.pipe(fs.createWriteStream('/dev/null')); //`${res.vid}.mp4`));
    console.log("Downloading...");
    
    video.on('progress', (chunkLength, downloaded, total) => {
      if (downloading === false) {
        downloading = true;
        bar1.start(total, downloaded);
      } else {
        bar1.update(downloaded);
      }
    });
    
    video.on('end', () => {
      bar1.stop();
      downloading = false;
    });
  }
}, sched2);
