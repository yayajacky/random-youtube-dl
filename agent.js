const _progress = require('cli-progress');
const request = require('request-promise');
// request.debug = true;
const fs = require('fs');
const ytdl = require('ytdl-core');
const later = require('later');
  
const textSched = later.parse.text('every 10 sec');
  
let downloading = false;
const bar1 = new _progress.Bar({}, _progress.Presets.shades_classic);

const getVidList = () => {
  return request({
    uri: "http://chatbot.catalinalabs.net/video/list",
    json: true,
  });
};

const markVidAsComplete = (vid) => {
  return request({
    method: 'POST',
    uri: "http://chatbot.catalinalabs.net/video/mark_as_complete",
    headers: {
      'Content-Type': 'application/json',
    },
    json: true,
    body: {
      vid,
    }
  });
};

const checkLink = (vid, options) => {
  return new Promise((resolve, reject) => {
    ytdl.getInfo(vid, options, (err, info) => {
      if (err) {
        reject(err);
      } else {
        resolve(info);
      }
    });
  });
};

later.setInterval(() => {
  if (downloading)
    return;
  
  // Check chatbot.catalinalabs.net/video/list for vidList
  // If empty, return, otherwise, try to download the first item via youtube
  // After download is finished, post to chatbot.catalinalabs.net/video/mark_as_complete {
  //   vid: 'id'
  // }
  const res = {};
  
  getVidList()
  .then((vidList) => {
    if (vidList.length === 0){
      console.log("Queue empty, nothing to download");
      return;
    }
    res.vid = vidList[Math.floor(Math.random() * vidList.length)];
    if (res.vid) {
      console.log(res);
      return checkLink(
        `http://www.youtube.com/watch?v=${res.vid}`, 
        { filter: (format) => format.container === 'mp4' }
      )
      .catch((err) => {
        // Something wrong with the video
        console.log("Not a valid youtube url, marking it as complete");
        markVidAsComplete(res.vid);
        return;
      });
    }
  })
  .then((info) => {
    if (info === undefined)
      return;
    // console.log(info);
    const video = ytdl(
      `http://www.youtube.com/watch?v=${res.vid}`,
      { filter: (format) => format.container === 'mp4' }
    )
    video
      .pipe(fs.createWriteStream('/dev/null'));
    
    console.log("Downloading...");
    video.on('progress', (chunkLength, downloaded, total) => {
      if (downloading === false) {
        downloading = true;
        bar1.start(total, downloaded);
      } else {
        bar1.update(downloaded);
      }
    });
    
    video.on('end', () => {
      bar1.stop();
      downloading = false;
      markVidAsComplete(res.vid);
    });
  })
  .catch((err) => {
    console.log("Something went wrong " + err.message);
  });
}, textSched);
